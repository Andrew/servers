Run [`just`][just] to list available commands:

```console
$ just
Available recipes:
    deploy environment *args   # Run the playbook for a given environment
    encrypt name               # Encrypt a named secret from stdin
    example-down               # Stop the example server
    example-up                 # Start the example server
    lint                       # Check the playbook for correctness
    list-inventory environment # List the servers in a given environment
    list-tags environment      # List the playbook tags for a given environment
    roles                      # Download third-party roles from Ansible Galaxy
```

[just]: https://github.com/casey/just
