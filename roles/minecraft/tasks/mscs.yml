# Adapted from https://minecraftservercontrol.github.io/docs/mscs/installation

- name: MSCS dependencies
  become: true
  ansible.builtin.package:
    name:
      - git
      - libjson-perl
      - liblwp-protocol-https-perl
      - libwww-perl
      - make
      - openjdk-19-jdk-headless
      - perl
      - python3
      - rdiff-backup
      - rsync
      - socat
      - util-linux
      - wget
  notify: clear package cache

- name: MSCS user
  become: true
  ansible.builtin.user:
    name: minecraft
    system: true
    password: "*" # Disabled
    shell: /bin/bash
    home: /opt/mscs
    umask: u=rwx,go=rx

- name: MSCS source
  become: true
  become_user: minecraft
  ansible.builtin.git:
    accept_newhostkey: true
    repo: https://github.com/MinecraftServerControl/mscs.git
    remote: MinecraftServerControl
    version: d18feeafed8e38cc7b7287db4b10eede41c50dee
    depth: 1
    dest: ~/src/mscs

- name: msctl command
  become: true
  ansible.builtin.copy:
    remote_src: true
    src: ~minecraft/src/mscs/msctl
    dest: /usr/local/bin/msctl
    mode: u=rwx,go=rx

- name: mscs command
  become: true
  ansible.builtin.copy:
    remote_src: true
    src: ~minecraft/src/mscs/mscs
    dest: /usr/local/bin/mscs
    mode: u=rwx,go=rx

- name: mscs command completion
  become: true
  ansible.builtin.copy:
    remote_src: true
    src: ~minecraft/src/mscs/mscs.completion
    dest: /etc/bash_completion.d/mscs
    mode: u=rw,go=r

- name: MSCS service template
  become: true
  ansible.builtin.copy:
    remote_src: true
    src: ~minecraft/src/mscs/mscs.service
    dest: /etc/systemd/system/mscs.service
    mode: u=rw,go=r
  notify: reload systemd

- name: MSCS systemd service drop-in directory
  become: true
  ansible.builtin.file:
    path: /etc/systemd/system/mscs.service.d
    state: directory
    mode: u=rwx,go=rx

- name: MSCS systemd service failure alerts
  become: true
  ansible.builtin.copy:
    dest: /etc/systemd/system/mscs.service.d/10-alert.conf
    mode: u=rw,go=r
    content: |
      [Unit]
      OnFailure=alert@%N.service
  notify: reload systemd

- name: MSCS service
  become: true
  ansible.builtin.service: { name: mscs, enabled: true }

- name: default MSCS configuration
  become: true
  become_user: minecraft
  ansible.builtin.command:
    creates: ~/mscs.defaults
    cmd: mscs help

- name: MSCS configuration
  become: true
  become_user: minecraft
  ansible.builtin.lineinfile:
    path: ~/mscs.defaults
    regexp: ^(?:#\s*)?{{ item.key }}=
    line: "{{ item.key }}={{ item.value }}"
  loop: "{{ properties | dict2items }}"
  vars:
    properties:
      mscs-default-initial-memory: "{{ minecraft_jvm_memory_initial }}"
      mscs-default-maximum-memory: "{{ minecraft_jvm_memory_max }}"
      mscs-default-jvm-args: "{% if minecraft_jvm_extra_args is defined %}{{ minecraft_jvm_extra_args | join(' ') }}{% endif %}"
