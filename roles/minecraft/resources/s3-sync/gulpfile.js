const { src } = require("gulp");
const awspublish = require("gulp-awspublish");
const rename = require("gulp-rename");
const { join } = require("path");

const destinationPattern = /^(?<region>[^:]+):(?<bucket>[^/]+)\/(?<prefix>.*)$/;

exports.default = () => {
  const { source, destination } = process.env;
  const { bucket, prefix, region } = destination.match(destinationPattern).groups;

  const publisher = awspublish.create(
    { params: { Bucket: bucket }, region },
    { cacheFileName: join(source, ".cache/s3.json") }
  );

  return src(["**/*", "!.cache/**"], { cwd: source })
    .pipe(rename((path) => { path.dirname = join(prefix, path.dirname); }))
    .pipe(publisher.publish({ "Cache-Control": "public, max-age=3600, stale-while-revalidate=2592000, stale-if-error=2592000" }))
    .pipe(publisher.sync(prefix))
    .pipe(publisher.cache())
    .pipe(awspublish.reporter());
};
